# JZJPipelinedCoreA

My 1st RISC-V cpu implementation with pipelining (RV32I)!

This cpu is largely different then JZJCoreA-C in terms of code (not just a fork). It will be my first truly pipelined cpu (probably 5ish stages) and so I will be focusing on completion rather than performance (like the focus for JZJCoreA). JZJPipelinedCoreB will be when I start focusing more on performance by better balancing combinational logic stages of the pipeline, doing branch prediction and speculative execution and other fancy stuff. For now, I fully expect JZJPipelinedCoreA to be slower than the very optimized JZJCoreC because I only plan to have 1 instruction at a time in the pipeline for now (though it could potentially have a higher fmax if my pipeline divisions are semidecent). JZJPipelinedCoreB will be the "performance catch up" core and by JZJPipelinedCoreC I should definitely be outperforming JZJCoreC.

*Future John here: Although this core has taken very little advantage of pipelining initially, it is already outperforming 2 cycle JZJCoreC instruction. There is still more ground to make up for 1 cycle instruction, but things look promising

Like other cores, my goal is to make this cpu compatible with the standard JZJCore memory layout (it should not be too difficult). Zifencei might be a problem though so I don't know if that will be supported.

Note: I am ditching JZJCoreD: the JZJCoreX series will be for single cycle or 2 way pipelined cpus (instruction fetch during execution). This is my first definitively "pipelined" cpu with 5 stages planned so it deserves a new naming scheme.

# Cycle counts for instructions

No idea how to measure this or an alternative metric for performance. (Maybe I can try to determine average CPI, which would be around 5 for this core for each instruction).

## Memory Map

Note: addresses are inclusive, bounds not specified are not mapped to anything, and execution starts at 0x00000000

| Bytewise Address (whole word) | Physical Word-wise Address | Function |
|:------------------------------|:---------------------------|:---------|
|0x00000000 to 0x00000003|0x00000000|RAM Start|
|0x0000FFFC to 0x0000FFFF|0x00003FFF|RAM End|
|0xFFFFFFE0 to 0xFFFFFFE3|0x3FFFFFF8|Memory Mapped IO Registers Start|
|0xFFFFFFFC to 0xFFFFFFFF|0x3FFFFFFF|Memory Mapped IO Registers End|
