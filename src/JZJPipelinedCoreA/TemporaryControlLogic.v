//The purpose of this module is to control the 5 stages in a manner so that only 1 instruction is in the pipeline at a given moment
//This makes this module "temporary" because in later cores the control logic will have to be able to have multiple instructions in the
//pipeline at a given instant
//For now this is good for seeing the benefits to Fmax by pipelining, and later cpi will be improved
module TemporaryControlLogic
(
	input clock,
	input reset,
	output reg initialize = 1'b0
);
/* Initialization Logic */

reg initialized = 1'b0;

always @(negedge clock, posedge reset)
begin
	if (reset)
	begin
		initialize <= 1'b0;
		initialized <= 1'b0;
	end
	else if (~clock)
	begin
		if (initialized == 1'b0)
		begin
			if (initialize == 1'b0)
				initialize <= 1'b1;//Begin initialization
			else//initialize == 1'b1
			begin//End initialization
				initialize <= 1'b0;
				initialized <= 1'b1;//Don't toggle pin again
			end
		end
	end
end

endmodule