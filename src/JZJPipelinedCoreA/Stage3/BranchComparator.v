module BranchComparator//Purely combinational
(
	input [31:0] rs1,
	input [31:0] rs2,
	input [2:0] funct3,
	
	input branchInstruction,//Signal later stages to just add 4 (force branchTaken to 0 unless unconditionalBranch is set)
	input unconditionalBranch,//jal/jalr; force branchTaken to 1
	
	output branchTaken
);
/* Wires */

reg branchTakenIntermediate;
assign branchTaken = branchTakenIntermediate | unconditionalBranch;

/* Comparison Logic */

always @*
begin
	if (branchInstruction)
	begin
		case (funct3)
			3'b000: branchTakenIntermediate = rs1 == rs2;//beq
			3'b001: branchTakenIntermediate = rs1 != rs2;//bne
			3'b100: branchTakenIntermediate = $signed(rs1) < $signed(rs2);//blt
			3'b101: branchTakenIntermediate = $signed(rs1) >= $signed(rs2);//bge
			3'b110: branchTakenIntermediate = rs1 < rs2;//bltu
			3'b111: branchTakenIntermediate = rs1 >= rs2;//bgeu
			default: branchTakenIntermediate = 1'bx;//Invalid funct 3
		endcase
	end
	else
	begin
		branchTakenIntermediate = 1'b0;
	end
end

endmodule