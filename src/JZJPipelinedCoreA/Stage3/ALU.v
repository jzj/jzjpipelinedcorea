module ALU//Purely combinational
(
	//Control lines
	input [2:0] funct3,
	input forceAdd,//Usefull for lui, auipc, and memory access instructions; ignores funct3 and mod and just adds x and y
	input mod,//Modifies the operation (add -> sub, srl -> sra)
	input zeroLSB,//For jalr; zeroes the lsb of the result

	//Inputs
	input [31:0] x,
	input [31:0] y,
	
	//Result
	output [31:0] result
);
/* Wires and Assignments */

reg [31:0] intermediateResult;
assign result[31:1] = intermediateResult[31:1];
assign result[0] = !zeroLSB & intermediateResult[0];

/* Logic */

always @*
begin
	if (forceAdd)
		intermediateResult = x + y;
	else
	begin
		case (funct3)
			3'b000: intermediateResult = mod ? x - y : x + y;//addi/add/sub
			3'b001: intermediateResult = x << y[4:0];//slli/sll
			3'b010: intermediateResult = ($signed(x) < $signed(y)) ? 32'h00000001 : 32'h00000000;
			3'b011: intermediateResult = (x < y) ? 32'h00000001 : 32'h00000000;
			3'b100: intermediateResult = x ^ y;
			3'b101: intermediateResult = mod ? (x >>> y[4:0]) : (x >> y[4:0]);
			3'b110: intermediateResult = x | y;
			3'b111: intermediateResult = x & y;
			default: intermediateResult = 32'hxxxxxxxx;//Should never happen
		endcase
	end
end

endmodule