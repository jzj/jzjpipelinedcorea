//Usefull refrences
//https://zipcpu.com/blog/2017/08/14/strategies-for-pipelining.html
//https://ai.berkeley.edu/~cs61c/fa17/lec/13/L13%20Pipelining%20(1up).pdf
//en.wikipedia.org/wiki/Classic_RISC_pipeline

//Note: Heavily based on JZJCoreC and the remains of JZJCoreD
module JZJPipelinedCoreA
#(
	parameter INITIAL_MEM_CONTENTS = "initialRam.mem",//File containing initial ram contents (32 bit words); execution starts from address 0x00000000
	//parameter RAM_A_WIDTH = 12//number of addresses for code/ram (not memory mapped io); 2^RAM_A_WIDTH words = 2^RAM_A_WIDTH * 4 bytes
	parameter RAM_A_WIDTH = 11//number of addresses for code/ram (not memory mapped io); 2^RAM_A_WIDTH words = 2^RAM_A_WIDTH * 4 bytes//FIXME for some reason this is taking up double the ram it should
)
(
	input clock,
	input reset,
	
	//CPU memory mapped io ports (for io dir, 0 = read, 1 = write)
	//Note that reads and writes are written to the addresses in little endian format
	//then converted back to be output / vise-versa for inputs
	//This makes it so for reads rd[0] = portXMemoryAddress[24] = portX[0]
	//and for writes............rs2[0] = portXMemoryAddress[24] = portX[0]
	//My recomendation is therefore that ports are accessed whole words at a time
	//but if you keep the little endian -> big endian format in mind you can write half words or bytes
	//Addresses for port read/write | io direction (0 for input, 1 for output)
	inout [31:0] portA,//FFFFFFF0   |   FFFFFFE0
	inout [31:0] portB,//FFFFFFF4   |   FFFFFFE4
	inout [31:0] portC,//FFFFFFF8   |   FFFFFFE8
	inout [31:0] portD,//FFFFFFFC   |   FFFFFFEC
	
	//Exposed port direction registers: Usefull for external modules to avoid multiple driver problems during synthesis
	//portX[Y] can only be written to by an external module if it is not in output mode
	//if portXDirection[y] is 1 then that pin is outputting data, and if portXDirection[y] is 0 then that pin is high impedance
	output [31:0] portADirection,
	output [31:0] portBDirection,
	output [31:0] portCDirection,
	output [31:0] portDDirection,
	
	//Output for legacy asembly test programs that output to register 31 (from before mmio was implemented); for new software use memory mapped io instead
	output [31:0] register31Output
);
/* Stage Connections */

//Stage 1
//Inputs
wire stage1Init;
wire stage1LatchNewPC;
wire [31:0] stage1NewPC;
wire [31:0] stage1InstructionIn;
//Outputs
wire [29:0] stage1InstructionMemoryAddress;
wire [31:0] stage1PCOut;
wire pcMisalignedErrorFlag;
wire [31:0] stage1InstructionOut;
wire stage1InstructionIsValid;//Not usefull now because of TemporaryControlLogic, but will be in the future to signal a nop when there are multiple instructions in the pipeline

//Stage 2
//Inputs
wire [31:0] stage2RS1In;
wire [31:0] stage2RS2In;
wire [31:0] stage2PCOfInstructionIn = stage1PCOut;
wire [31:0] stage2InstructionIn = stage1InstructionOut;
wire stage2InstructionIsValidIn = stage1InstructionIsValid;
//Outputs
wire [31:0] stage2InstructionOut;
wire stage2InstructionIsValidOut;
wire [31:0] stage2PCOfInstructionOut;
wire [31:0] stage2RS1Out;
wire [31:0] stage2RS2Out;

//Stage 3
//Inputs
wire [31:0] stage3InstructionIn = stage2InstructionOut;
wire stage3InstructionIsValidIn = stage2InstructionIsValidOut;
wire [31:0] stage3PCOfInstructionIn = stage2PCOfInstructionOut;
wire [31:0] stage3RS1In = stage2RS1Out;
wire [31:0] stage3RS2In = stage2RS2Out;
//Outputs
wire [31:0] stage3InstructionOut;
wire stage3InstructionIsValidOut;
wire [31:0] stage3PCOfInstructionOut;
wire [31:0] stage3ResultOut;
wire [31:0] stage3RS2Out;
wire stage3BranchTakenOut;

//Stage 4
//Inputs
wire [31:0] stage4InstructionIn = stage3InstructionOut;
wire stage4InstructionIsValidIn = stage3InstructionIsValidOut;
wire [31:0] stage4PCOfInstructionIn = stage3PCOfInstructionOut;
wire [31:0] stage4ResultIn = stage3ResultOut;
wire [31:0] stage4RS2In = stage3RS2Out;
wire stage4BranchTakenIn = stage3BranchTakenOut;
//Outputs
wire [31:0] stage4InstructionOut;
wire stage4InstructionIsValidOut;
wire [31:0] stage4PCOfInstructionOut;
wire [31:0] stage4NextPCOut;
wire [31:0] stage4NextSeqPCOut;
wire [31:0] stage4ResultFrom3Out;
wire [31:0] stage4MemoryOut;
wire [31:0] stage4RS2Out;
assign stage1LatchNewPC = stage4InstructionIsValidOut;//It is ok that the next instruction is fetched early because the registers being used in stage 5 are not latched until stage2

//Stage 5
//Inputs
wire [31:0] stage5InstructionIn = stage4InstructionOut;
wire stage5InstructionIsValidIn = stage4InstructionIsValidOut;
wire [31:0] stage5PCOfInstructionIn = stage4PCOfInstructionOut;
wire [31:0] stage5NextPCIn = stage4NextPCOut;
wire [31:0] stage5NextSeqPCIn = stage4NextSeqPCOut;
wire [31:0] stage5ResultFrom3In = stage4ResultFrom3Out;
wire [31:0] stage5MemoryIn = stage4MemoryOut;
wire [31:0] stage5RS2In = stage4RS2Out;

/* Other Module Connections */
//Register File
wire registerFileWE;
wire [31:0] registerFileIn;
wire [4:0] registerFileRDAddress;
wire [4:0] registerFileRS1Address;
wire [4:0] registerFileRS2Address;

//Memory backend
wire memoryBackendWE;
wire [29:0] memoryBackendWriteAddress;
wire [29:0] memoryBackendReadAddress;
wire [31:0] memoryBackendDataIn;
wire [31:0] memoryBackendDataOut;

/* Stage Modules */

//Instruction fetch
Stage1 stage1 (.clock(clock), .reset(reset), .initialize(stage1Init), .newPC(stage1NewPC), .newPCIsValid(stage1LatchNewPC), .instructionIn(stage1InstructionIn),
					.isValid(stage1InstructionIsValid), .instructionOut(stage1InstructionOut), .instructionAddressToAccess(stage1InstructionMemoryAddress), .pcOut(stage1PCOut),
					.pcMisaligned(pcMisalignedErrorFlag));

//Instruction decode
Stage2 stage2 (.clock(clock), .reset(reset), .rs1Address(registerFileRS1Address), .rs2Address(registerFileRS2Address), .rs1In(stage2RS1In), .rs2In(stage2RS2In),
					.pcOfInstructionIn(stage2PCOfInstructionIn), .instructionIn(stage2InstructionIn), .instructionIsValidIn(stage2InstructionIsValidIn), .instructionOut(stage2InstructionOut),
					.instructionIsValidOut(stage2InstructionIsValidOut), .pcOfInstructionOut(stage2PCOfInstructionOut), .rs1Out(stage2RS1Out), .rs2Out(stage2RS2Out));
					
//Execute
Stage3 stage3 (.clock(clock), .reset(reset), .instructionIn(stage3InstructionIn), .instructionIsValidIn(stage3InstructionIsValidIn), .pcOfInstructionIn(stage3PCOfInstructionIn),
					.rs1In(stage3RS1In), .rs2In(stage3RS2In), .instructionOut(stage3InstructionOut), .instructionIsValidOut(stage3InstructionIsValidOut),
					.pcOfInstructionOut(stage3PCOfInstructionOut), .resultOut(stage3ResultOut), .rs2Out(stage3RS2Out), .useResultAsNextPC(stage3BranchTakenOut));
					
//Memory read
Stage4 stage4 (.clock(clock), .reset(reset), .instructionIn(stage4InstructionIn), .instructionIsValidIn(stage4InstructionIsValidIn), .pcOfInstructionIn(stage4PCOfInstructionIn),
					.rs2In(stage4RS2In), .executeResultIn(stage4ResultIn), .useResultAsNextPCIn(stage4BranchTakenIn), .instructionOut(stage4InstructionOut),
					.instructionIsValidOut(stage4InstructionIsValidOut), .pcOfInstructionOut(stage4PCOfInstructionOut), .nextPCOut(stage1NewPC), .nextSeqPCOut(stage4NextSeqPCOut),
					.resultOut(stage4ResultFrom3Out), .memoryOut(stage4MemoryOut), .rs2Out(stage4RS2Out), .memoryAddress(memoryBackendReadAddress), .memoryDataIn(memoryBackendDataOut));
					
//Memory/register write
Stage5 stage5(.clock(clock), .reset(reset), .instructionIn(stage5InstructionIn), .instructionIsValidIn(stage5InstructionIsValidIn), .pcOfInstructionIn(stage5PCOfInstructionIn),
				  .nextSeqPCIn(stage5NextSeqPCIn), .resultIn(stage5ResultFrom3In), .memoryIn(stage5MemoryIn), .rs2In(stage5RS2In), .memoryAddress(memoryBackendWriteAddress),
				  .memoryWE(memoryBackendWE), .memoryDataToWrite(memoryBackendDataIn), .rdAddress(registerFileRDAddress), .rd(registerFileIn), .rdWE(registerFileWE));

/* Other Modules */

//Memory Backend
MemoryBackend #(.INITIAL_RAM_CONTENTS(INITIAL_MEM_CONTENTS), .A_WIDTH(RAM_A_WIDTH)) memoryBackend
					(.clock(clock), .reset(reset), .writeAddress(memoryBackendWriteAddress), .readAddress(memoryBackendReadAddress), .dataIn(memoryBackendDataIn), .writeEnable(memoryBackendWE),
					 .dataOut(memoryBackendDataOut), .instructionOut(stage1InstructionIn), .instructionAddress(stage1InstructionMemoryAddress), .mmPortA(portA), .mmPortB(portB),.mmPortC(portC),
					 .mmPortD(portD), .mmPortADirection(portADirection), .mmPortBDirection(portBDirection), .mmPortCDirection(portCDirection), .mmPortDDirection(portDDirection));
					 
//Register File
RegisterFile registerFile (.clock(clock), .reset(reset), .dataIn(registerFileIn), .rd(registerFileRDAddress), .writeEnable(registerFileWE), .rs1(registerFileRS1Address),
									.rs2(registerFileRS2Address), .portRS1(stage2RS1In), .portRS2(stage2RS2In), .register31Output(register31Output));
									
TemporaryControlLogic tempControlLogic(.clock(clock), .reset(reset), .initialize(stage1Init));

endmodule
