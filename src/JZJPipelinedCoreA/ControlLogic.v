//Controls stalling stages based on memory addresses/registers being accessed
//Handles data hazards; does not handle control hazards (that should be passed to Stage1
//from Stage2 or Stage3)
//If a hazard is detected, the control logic outputs a pause signal which multiplexes a stage's output to a nop and
//disables the stage from recieving input
//Stage1 also has a seperate thing going on that pauses unless it has a proper PC, but that is not handled in this
//module
module ControlLogic
(
	//Instructions being executed and other data at each stage for comparison
	input [31:0] instructionStage2,//Look at registers being read in decode
	input [31:0] instructionStage3,//Look at registers being used in execute (not written back yet)
	input [31:0] instructionStage4,//Look at registers passing through stage 4 (not written back)
	input [31:0] instructionStage5,//Look at registers being written in writeback
	input [29:0] memoryAddressStage4,//Look at the memory location being read in stage 4
	input [29:0] memoryAddressStage5,//Look at the memory location being written in writeback
	
	//Stage pausing outputs
	output stage1Pause = 1'b0,
	output reg stage2Pause,
	output reg stage3Pause,
	output reg stage4Pause,
	output reg stage5Pause
);

/* Hazard Detection Logic */

wire memoryAddressHazard = memoryAddressStage4 == memoryAddressStage5;
reg rs1Hazard;
reg rs2Hazard;

always @*
{
	case ()
		
		
		default: registerHazard = 1'b0;
	endcase
}

/* Pause Logic */

always @*
{
	
}

/* Instruction Register Parameter Detection Functions */

function automatic hasRS2(input [31:0] instruction);
begin
	case (instruction[6:0])
		7'b1100011: hasRS2 = 1'b1;
		7'b0100011: hasRS2 = 1'b1;
		7'b0110011: hasRS2 = 1'b1;
		default: hasRS2 = 1'b0;
	endcase
end
endfunction

function automatic hasRS1(input [31:0] instruction);
begin
	case (instruction[6:0])
		7'b0110111: hasRS1 = 1'b0;
		7'b0010111: hasRS1 = 1'b0;
		7'b1101111: hasRS1 = 1'b0;
		7'b1101111: hasRS1 = 1'b0;
		7'b0001111: hasRS1 = 1'b0;//Technically fence does have rs1 but this cpu is so simple it dosen't matter anyways
		7'b1110011: hasRS1 = 1'b0;
		default: hasRS1 = 1'b1;
	endcase
end
endfunction

function automatic hasRD(input [31:0] instruction);
begin
	case (instruction[6:0])
		7'b1100011: hasRD = 1'b0;
		7'b0100011: hasRD = 1'b0;
		7'b0001111: hasRD = 1'b0;//Technically fence does have rd but this cpu is so simple it dosen't matter anyways
		7'b1110011: hasRD = 1'b0;
		default: hasRD = 1'b1;
	endcase
end
endfunction

endmodule