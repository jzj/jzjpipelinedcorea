module Stage1
(
	input clock,
	input reset,
	
	input initialize,
	
	//PC setting stuffs
	input [31:0] newPC,
	input newPCIsValid,//If 1, newPC is latched into the internal pc, the externally facing pcOut register and the instruction is fetched and output at that address on the next posedge
	
	//Stage 1 outputs
	output reg [31:0] pcOut,//This address corresponds to the address of instructionOut
	output [31:0] instructionOut,
	output reg isValid = 1'b0,//If 0, interpret the pcOut and the instructionOut as a nop
	
	//Memory access
	output reg [29:0] instructionAddressToAccess,
	input [31:0] instructionIn,
	
	//Error flag
	output pcMisaligned
);
/* Registers, Wires, and Assignments */

reg [31:0] internalPCRegister = 32'h00000000;
assign pcMisaligned = (internalPCRegister % 4) != 0;//program counter is not aligned to a 4 byte address

/* pcOut and isValid Multiplexing/Bypassing */

always @(posedge clock, posedge reset)
begin
	if (reset)
	begin
		pcOut <= 1'b0;
		isValid <= 1'b0;
	end
	else if (clock)
	begin
		if (newPCIsValid)
		begin
			pcOut <= newPC;//Bypass internalPCRegister
			isValid <= 1'b1;//We're fetching a new instruction, so it will be valid
		end
		else
		begin
			pcOut <= internalPCRegister;
			isValid <= 1'b0 | initialize;//No new instruction yet; if we're initializing, then the instruction we are outputing (32'h00000000)...
			//... must be marked as valid because it is the first instruction and will kickstart the pipeline
		end
	end
end

/* Instruction Fetching Logic and Output Multiplexing/Bypassing */

always @*
begin
	if (newPCIsValid)
		instructionAddressToAccess = newPC[31:2];//Bypass internalPCRegister in order to fetch the instruction on the same posedge
	else
		instructionAddressToAccess = internalPCRegister[31:2];//No new instruction; no need to bypass
end

assign instructionOut = toBigEndian(instructionIn);//updated each posedge by the memory

/* internalPCRegister Latching Logic */

always @(posedge clock, posedge reset)
begin
	if (reset)
		internalPCRegister <= 32'h00000000;
	else if (clock)
	begin
		if (newPCIsValid)
			internalPCRegister <= newPC;
	end
end

/* External Stuff */

//Endianness functions
`include "JZJPipelinedCoreA/Memory/EndiannessFunctions.v"

endmodule